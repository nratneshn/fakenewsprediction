import math
import pandas as pd

 
def train_test_split(dataset, splitRatio):
	"""
	given dataset and a split ratio, splits the dataset into 
	train and test datasets
	"""
	train_dataset = dataset.sample(frac=splitRatio, replace=False, random_state=1)
	indices = train_dataset.index.tolist()
	test_dataset = dataset.drop(indices)
	return train_dataset, test_dataset
	
def guassian_distribution(x, mu, sigma):
	"""
	Calcuates the normal distribution probability given, xm mean and standard deviation
	returns the probability
	"""
	guassian = (1 / (math.sqrt(2*math.pi * sigma * sigma))) * (math.exp(-(math.pow(x-mu,2)/(2*sigma*sigma))))
	return guassian
 	
def class_probabilities(train_dataset):
	"""
	calculate the probabilites for each class from the train data
	"""
	class_details = {}
	# get all classes that exist(0 and 1 in this case)
	classes = train_dataset['label'].unique().tolist()
	# get list of features
	feature_list = list(train_dataset)
	# remove label from feature
	feature_list.remove('label')
	for feature in feature_list:
		# checking if there is atleast 3 unique values for standard deviation to not be zero
		if len(train_dataset[feature].unique().tolist()) < 4:
			train_dataset.drop(train_dataset[feature], axis=1)	
	
	for each_class in classes:
		# only get the details for the specific class
		class_dataset = train_dataset.loc[train_dataset['label'] == each_class]
		# remove the label as it's not neccessary for prediction calculation
		class_dataset.drop(class_dataset.columns[len(class_dataset.columns)-1], axis=1, inplace=True)
		# will contains mean and standard deviation corresponding to each feature
		feature_summary = []
		
		for each_feature in feature_list:
			# calculate the mean and std for each features and append to summary
			mu = class_dataset[each_feature].mean()
			sigma = class_dataset[each_feature].std()
			feature_summary.append([each_feature, mu, sigma])
		# converting feature_summary to dataframe for easier manipulations
		class_info = pd.DataFrame(feature_summary, columns=['features', 'mu', 'sigma'])
		# store dataframe containing feature summary for each class
		# the class value being the key
		class_details[each_class] = class_info
		
	return class_details			

def check_probability(feature_details, test_case):
	"""
	"""
	probabilities = {}
	# for each label (0/1)
	for label in feature_details:
		# for each features of the test row
		for column in test_case:
			# check if feature is in the feature summary
			if column in list(feature_details[label]['features']):
				# get the value of x for the test case
				x = test_case[column].values[0]
				metadata = feature_details[label].loc[feature_details[label]['features'] == column]
				# get the mean and sigma for the specific feature to calculate probability
				mu = metadata["mu"].values[0]
				sigma = metadata["sigma"].values[0]
				# if class already has some probability then multiply
				# see project documentation for more details on Naive Bayes implementation
				if label in probabilities.keys():
					probabilities[label] *= guassian_distribution(x, mu, sigma)
				else:
					probabilities[label] = guassian_distribution(x, mu, sigma)
	
	return probabilities
	
def obtain_prediction(class_details, test_dataset):
	"""
	predict label for each test case given
	"""
	predictions = []
	# for each test case
	for i in range(len(test_dataset)):
		# locate the test case info in the test dataset
		curr_data = test_dataset.iloc[[i]]
		# calculate each class probability for this test case
		each_probabilities = check_probability(class_details, curr_data)
		# get maximum key value
		key_max = max(each_probabilities, key=each_probabilities.get)
		predictions.append(key_max)	
	
	return predictions
 
def test_accuracy(predictions, actual_labels):
	"""
	Test how accurate the prediction was to the test cases labels
	"""
	accurate = 0
	for i in range(len(actual_labels)):
		if actual_labels[i] == predictions[i]:
			accurate += 1
	return (accurate/len(actual_labels)) * 100.0
 
 
 
if __name__ == '__main__':
	# get data from file and store in dataframe
	dataset = pd.read_csv('preprocessed_data.csv')
	# make sure all values are numeric
	dataset = dataset.apply(pd.to_numeric, errors='ignore')
	# The ration to split the train and test data by
	# ex, 0.4 indicate, 40% train, 60% test
	splitRatio = 0.999
	# Actually split data into according parts
	train_dataset, test_dataset = train_test_split(dataset, splitRatio)
	# output split details
	print("Split details:\n Total Size: "+str(len(dataset))+"\n Train Size: "+str(len(train_dataset))+"\n Test Size: "+str(len(test_dataset))+"")
	# calculate probability of each class (fake/real) given training data 
	probabilities = class_probabilities(train_dataset)
	# predict based off probability and test data
	predictions = obtain_prediction(probabilities, test_dataset)
	# check the accuracy given the predicted results and results from test sample
	print(predictions)
	print(test_dataset['label'].tolist())
	accuracy = test_accuracy(predictions, test_dataset['label'].tolist())
	print(accuracy)
