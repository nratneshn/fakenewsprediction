import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)



if __name__ == '__main__':

    # Obtaining preprocessed data
    df = pd.read_csv('preprocessed_data.csv')
    df['label'] = pd.to_numeric(df['label'])
    # Setting up y vector
    y = df['label'].values
    # creating feature columns by removing the label
    df.drop(df.columns[len(df.columns)-1], axis=1, inplace=True)
    # setting up feature matrix
    X = df.values

    # Splitting the dataset into the Training set and Test set
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

	# Fitting Naive Bayes to the Training set
    classifierNB = GaussianNB()
    classifierNB.fit(X_train, y_train)

	# Fitting Logistic Regression to the Training set
    classifierLR = LogisticRegression()
    classifierLR.fit(X_train, y_train)
	
	# Fitting Neural Network to the Training set
    mlp = MLPClassifier(hidden_layer_sizes=(40,40,40))
    mlp.fit(X_train,y_train)
	
    # Predicting the Test set results
    y_pred_NB = classifierNB.predict(X_test)
    y_pred_LR = classifierLR.predict(X_test)
    y_pred_NN = mlp.predict(X_test)

    # Making the Confusion Matrix
    cmNB = confusion_matrix(y_test, y_pred_NB)
    cmLR = confusion_matrix(y_test, y_pred_LR)
    cmNN = confusion_matrix(y_test, y_pred_NN)
	
    print(cmNB)
    print("Naive Bayes: \n"+str(accuracy_score(y_test, y_pred_NB, normalize=False)/(len(y_test))))
    print(cmLR)
    print("Logistic Regression: \n"+str(accuracy_score(y_test, y_pred_LR, normalize=False)/(len(y_test))))
    print(cmNN)
    print("Neural Network: \n"+str(accuracy_score(y_test, y_pred_NN, normalize=False)/(len(y_test))))

